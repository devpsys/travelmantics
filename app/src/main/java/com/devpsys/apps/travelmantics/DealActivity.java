package com.devpsys.apps.travelmantics;

import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import static com.devpsys.apps.travelmantics.FirebaseUtil.openFbReference;

public class DealActivity extends AppCompatActivity {

    private EditText txtPrice;
    private EditText txtTitle;
    private EditText txtDescription;
    private ImageView imageView;

    private DatabaseReference mDatabaseReference;

    private TravelDeal mDeal;
    private static final int PICTURE_RESULT = 9000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deal);

        txtPrice = findViewById(R.id.txtPrice);
        txtTitle = findViewById(R.id.txtTitle);
        txtDescription = findViewById(R.id.txtDescription);
        imageView = findViewById(R.id.image);

        openFbReference("traveldeals", this);

        mDatabaseReference = FirebaseUtil.mDatabaseReference;

        Intent intent = getIntent();
        TravelDeal deal = (TravelDeal) intent.getSerializableExtra("theDeal");

        if (deal == null)
            deal = new TravelDeal();
        mDeal = deal;

        txtTitle.setText(mDeal.getTitle());
        txtPrice.setText(mDeal.getPrice());
        txtDescription.setText(mDeal.getDescription());
        showImage(mDeal.getImageUrl());
        Button btnImage = findViewById(R.id.btnImage);
        btnImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                intent.setType("image/jpeg");
                intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                startActivityForResult(intent.createChooser(intent, "Insert Picture"), PICTURE_RESULT);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.save_menu, menu);
        if (FirebaseUtil.mIsAdmin) {
            findViewById(R.id.btnImage).setVisibility(View.VISIBLE);
            menu.findItem(R.id.delete_menu).setVisible(true);
            menu.findItem(R.id.save_menu).setVisible(true);
            enableEditTexts(true);
        } else {
            findViewById(R.id.btnImage).setVisibility(View.GONE);
            menu.findItem(R.id.delete_menu).setVisible(false);
            menu.findItem(R.id.save_menu).setVisible(false);
            enableEditTexts(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.save_menu:
                saveDeal();
                Toast.makeText(this, "Deal saved", Toast.LENGTH_SHORT).show();
                clean();
                backToList();
                return true;
            case R.id.delete_menu:
                deleteDeal();
                Toast.makeText(this, "Deal Deleted", Toast.LENGTH_SHORT).show();
                backToList();
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICTURE_RESULT && resultCode == RESULT_OK) {
            Uri imageUri = data.getData();
            final StorageReference ref =
                    FirebaseUtil.mStorageReference.child(imageUri.getLastPathSegment());

            UploadTask uploadTask = ref.putFile(imageUri);
            uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(final UploadTask.TaskSnapshot taskSnapshot) {
                    Task<Uri> firebaseUri = taskSnapshot.getStorage().getDownloadUrl();
                    firebaseUri.addOnSuccessListener(new OnSuccessListener<Uri>() {
                        @Override
                        public void onSuccess(Uri uri) {
                            String pictureName = taskSnapshot.getStorage().getPath();
                            String url = uri.toString();
                            mDeal.setImageUrl(url);
                            mDeal.setImageName(pictureName);
                            Log.d("Admin", url);
                            Log.d("Admin", pictureName);
                            showImage(url);
                        }
                    });
                }
            });

//            Task<Uri> urlTask = uploadTask.continueWithTask(
//                    new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
//                        @Override
//                        public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
//                            if (!task.isSuccessful()) {
//                                throw task.getException();
//                            }
//
//                            // Continue with the task to get the download URL
//                            return ref.getDownloadUrl();
//                        }
//                    }).addOnCompleteListener(new OnCompleteListener<Uri>() {
//                @Override
//                public void onComplete(@NonNull Task<Uri> task) {
//                    if (task.isSuccessful()) {
//                        String downloadUrl = task.getResult().toString();
//                        Toast.makeText(DealActivity.this,
//                                downloadUrl, Toast.LENGTH_SHORT).show();
//                    } else {
//                        // Handle failures
//                    }
//                }
//            });
        }
    }

    private void saveDeal() {
        mDeal.setPrice(txtPrice.getText().toString());
        mDeal.setTitle(txtTitle.getText().toString());
        mDeal.setDescription(txtDescription.getText().toString());

        if (mDeal.getId() == null)
            mDatabaseReference.push().setValue(mDeal);
        else
            mDatabaseReference.child(mDeal.getId()).setValue(mDeal);
    }

    private void deleteDeal() {
        if (mDeal == null) {
            Toast.makeText(this, "Please save the deal before deleting",
                    Toast.LENGTH_SHORT).show();
            return;
        }
        mDatabaseReference.child(mDeal.getId()).removeValue();

        if (mDeal.getImageName() != null && !mDeal.getImageName().isEmpty()) {
            StorageReference picRef = FirebaseUtil.mStorage.getReference().child(mDeal.getImageName());
            picRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("Admin", "Image Successfully deleted");
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.d("Admin", e.getMessage());
                }
            });
        }
    }

    private void backToList() {
        startActivity(new Intent(this, ListActivity.class));
        finish();
    }

    private void clean() {
        txtPrice.setText("");
        txtTitle.setText("");
        txtDescription.setText("");
        txtTitle.requestFocus();
    }

    private void enableEditTexts(boolean isEnable) {
        txtTitle.setEnabled(isEnable);
        txtPrice.setEnabled(isEnable);
        txtDescription.setEnabled(isEnable);
    }

    private void showImage(String url) {
        if (url != null && !url.isEmpty()) {
            int width = Resources.getSystem().getDisplayMetrics().widthPixels;
            Picasso.get()
                    .load(url)
                    .resize(width, width * 2 / 3)
                    .centerCrop()
                    .into(imageView);
        }
    }
}
